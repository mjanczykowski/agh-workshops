#pragma once

#include "framework/structs/segment.h"
#include "framework/structs/duty.h"
#include "framework/structs/pairing.h"

#include "framework/rule_engine.h"
#include "framework/cost_utility.h"


class SolutionValidator
{
public:
	SolutionValidator(const std::vector<Pairing *> & pairings, const std::vector<Segment *> & segments)
		: pairings_(pairings)
		, segments_(segments)
	{}

	bool validate_solution() const;
	double calculate_cost() const;
 
	bool validate_horizontal_rules_() const;
	bool validate_overcoverage_() const;

	std::vector<int> get_covered_operating_segments_() const;

	bool validate_base_constraint_() const;
	bool validate_vertical_rules_() const;

	unsigned int nbr_uncovered_segments_() const;

	std::vector<int> uncovered_segments_() const;
	double cost_uncovered_segments_() const;
	double cost_man_days_() const;
	double cost_layover_() const;

	long cost_layover(Pairing * pairing, long total_layover_length) const;

	double cost_dhd_() const;

	const RuleEngine & rule_engine() const { return rule_engine_; }
	const CostUtility & cost_utility() const { return cost_utility_; }

private:
	std::vector<Segment *> segments_;
	std::vector<Pairing *> pairings_;

	RuleEngine rule_engine_;
	CostUtility cost_utility_;

};