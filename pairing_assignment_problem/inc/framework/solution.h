#pragma once

#include <vector>
#include <sstream>

#include "framework/structs/pairing.h"
#include "framework/utils/io_segments.h"


class Solution
{

private:
    std::vector<Pairing *> _pairings;

public:
    void add_pairing(Pairing * pairing) { _pairings.push_back(pairing); }
    void set_solution(std::vector<Pairing *> pairings) { _pairings = pairings; }

    const std::vector<Pairing *> & get_pairings() const { return _pairings; }

    void write_to_file(std::string input_seg_filename) const
    {
        IOUtils::write_pairings(input_seg_filename, _pairings);
    }

    void print() const
    {
       IOUtils::print_pairings(_pairings);
    }

};