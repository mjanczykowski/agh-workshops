#pragma once

#include "framework/parameters.h"

#include "framework/structs/segment.h"
#include "framework/structs/duty.h"
#include "framework/structs/pairing.h"

// convert to namespace?

class RuleEngine {
	
public:
	RuleEngine(void);
	// TODO: add your methods here.

	bool is_valid(const Segment * const) const;
	bool is_valid(const Duty * const) const;
	bool is_valid(const Pairing * const) const;

	bool is_legal(const Duty * const) const;
	bool is_legal(const Pairing * const) const;

private:

	
};