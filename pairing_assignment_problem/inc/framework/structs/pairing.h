#pragma once

#include "framework/parameters.h"

#include "framework/structs/segment.h"
#include "framework/structs/duty.h"



class Pairing
{

public:
	Pairing() {}

	const std::string & base() const { return duties_.front()->dep_stn(); }

	unsigned int start_time() const { return duties_.front()->start_time(); }
	unsigned int end_time() const { return duties_.back()->end_time(); }

	unsigned int start_day() const { return static_cast<int>(std::floor(start_time() / MINUTES_PER_DAY)); }
	unsigned int end_day() const { return static_cast<int>(std::floor(end_time() / MINUTES_PER_DAY)); }

	size_t num_duties() const { return duties_.size(); }

	void add_duty(Duty * duty) { duties_.push_back(duty); }
	const std::vector<Duty *> & get_duties() const { return duties_; }

	unsigned int pairing_length() const { return end_time() - start_time(); }
	unsigned int num_days() const { return end_day() - start_day() + 1; }

	std::string get_string() const {

		std::stringstream str;
		str << "[ ";
		for (size_t i = 0; i < duties_.size(); ++i)
			str << duties_[i]->get_string() << " ";
		str << "]";
		return str.str();
	}

private:

	size_t id_;

	std::vector<Duty *> duties_;
};


