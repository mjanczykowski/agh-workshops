
#pragma once

#include <vector>

#include "framework/problem.h"
#include "framework/rule_engine.h"
#include "framework/cost_utility.h"
#include "framework/solution_validator.h"

class Station
{
public:

	Station(const std::string& name)
		: name(name)
	{ }

	void addSegment(Segment* segment)
	{
		segments.push_back(segment);
	}

	std::vector<Segment *>::iterator begin()
	{
		return segments.begin();
	}

	std::vector<Segment *>::iterator beginSince(unsigned int time)
	{
		auto element = new Segment(0, 0, "", "", time, 0, 0);
		return std::lower_bound(segments.begin(), segments.end(), element, segment_comp);
	}

	std::vector<Segment *>::iterator end()
	{
		return segments.end();
	}

private:
	const std::string name;
	std::vector<Segment*> segments;

	static bool segment_comp(const Segment* a, const Segment* b)
	{
		return a->dep_time() < b->dep_time();
	}
};

typedef std::map<std::string, Station*> StationsMap;

StationsMap getStationsFromProblem(const Problem& problem);

std::vector<Pairing *> solve(const Problem & problem);

std::vector<Duty> generateDuties(const Problem& problem, const StationsMap& stationsMap);