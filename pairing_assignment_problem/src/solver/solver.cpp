#include "stdafx.h"
#include "solver/solver.h"
#include <algorithm>

std::vector<Duty> generateDuties(const Problem& problem, const StationsMap& stationsMap)
{
  std::vector<Duty> duties;
  {
//    auto generated = generateDutiesAdam(problem, stationsMap);
//    std::copy(generated.begin(), generated.end(), std::back_inserter(duties));
  }

  return duties;
}

std::vector<Pairing *> solve(const Problem & problem)
{
  const StationsMap stationsMap = getStationsFromProblem(problem);

  auto duties = generateDuties(problem, stationsMap);
  for (auto& d: duties)
  {
    std::cout << d.get_string() << std::endl;
  }

  std::vector<Pairing *> solution;
  return solution;
}

StationsMap getStationsFromProblem(const Problem& problem)
{
	std::map<std::string, Station*> stations;

	auto segments = problem.get_segments();

	for (auto segment : segments)
	{
		auto dep_stn_name = segment->dep_stn();

		auto stn_it = stations.find(dep_stn_name);

		Station *station;

		if (stn_it != stations.end())
		{
			station = stn_it->second;
		}
		else
		{
			station = new Station(dep_stn_name);
			stations[dep_stn_name] = station;
		}

		station->addSegment(segment);
	}

	return stations;
}
