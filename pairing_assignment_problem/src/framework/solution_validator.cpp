#include "stdafx.h"
#include "framework/solution_validator.h"

#include "framework/parameters.h"


bool SolutionValidator::validate_solution() const
{
	return validate_horizontal_rules_() && validate_vertical_rules_();
}


double SolutionValidator::calculate_cost() const
{
	double total_cost = 0.0;
	total_cost += cost_uncovered_segments_();
	total_cost += cost_man_days_();
	total_cost += cost_layover_();
	total_cost += cost_dhd_();

	return total_cost;
}


bool SolutionValidator::validate_horizontal_rules_() const
{
	bool legal = true;
	for (Pairing * pairing : pairings_) {
		legal 
			= rule_engine_.is_valid(pairing) 
			&& rule_engine_.is_legal(pairing);
		if (!legal)
			break;
	}

	return legal;
}


// validate that each pairing is covered at most by one pairing
bool SolutionValidator::validate_overcoverage_() const
{
	std::vector<int> covered_segments = get_covered_operating_segments_();

	const size_t old_size = covered_segments.size();

	covered_segments.erase(
		std::unique(covered_segments.begin(), covered_segments.end()),
		covered_segments.end());

	const size_t new_size = covered_segments.size();

	return new_size == old_size;
}


std::vector<int> SolutionValidator::get_covered_operating_segments_() const
{
	// id's of covered segments
	std::vector<int> covered_segments; covered_segments.reserve(segments_.size());

	for (auto pairing : pairings_) {
		for (auto duty : pairing->get_duties()) {
			for (auto segment : duty->get_segments()) {
				if (segment->is_operating())
					covered_segments.push_back(segment->id());
			}
		}
	}

	return covered_segments;
}


bool SolutionValidator::validate_base_constraint_() const
{
	// actual count for each base number of pairings touching each day
	std::map < std::string, // base
		std::vector<int> > base_pairing_touching_each_day_count;

	// collect actual values
	{
		const int init_val = 0;
		for (const std::string & base : parameters::bases) {
			base_pairing_touching_each_day_count.insert(
				std::make_pair(base, std::vector<int>(parameters::window_length, init_val))
				);
		}

		for (auto pairing : pairings_) {
			const std::string & base = pairing->base();
			assert(pairing->end_day() < parameters::window_length);
			for (size_t day_cnt = pairing->start_day(); day_cnt <= pairing->end_day(); ++day_cnt) {
				base_pairing_touching_each_day_count[base][day_cnt] += 1;
			}
		}
	}

	// validate legality
	bool legal = true;
	for (const std::string & base : parameters::bases) {
		auto itr = vertical_constraints_limits::limit_base_constraint.find(base);

		// if limit for base does not exists it means there is no restriction
		if (itr == vertical_constraints_limits::limit_base_constraint.end())
			continue;

		const int limit = itr->second;
		for (int day_cnt = 0; day_cnt < parameters::window_length && legal; ++day_cnt) {
			if (base_pairing_touching_each_day_count[base][day_cnt] > limit)
				legal = false;
		}

		if (!legal)
			break;
	}

	return legal;
}


bool SolutionValidator::validate_vertical_rules_() const
{
	// check over-coverage
	return validate_overcoverage_()
		&& validate_base_constraint_();
}


unsigned int SolutionValidator::nbr_uncovered_segments_() const
{
	return uncovered_segments_().size();
}


std::vector<int> SolutionValidator::uncovered_segments_() const
{
	using namespace std::placeholders;

	std::vector<int> uncovered_segments;

	std::vector<Segment *> input_operating_segments(segments_.size(), nullptr);
	auto is_operating = std::bind(&Segment::is_operating, _1);
	auto last_el_itr = std::copy_if(
		segments_.begin(), segments_.end(),
		input_operating_segments.begin(), is_operating);

	input_operating_segments.erase(last_el_itr, input_operating_segments.end());

	std::vector<int> input_segments_ids;

	auto get_id = std::bind(&Segment::id, _1);
	std::transform(
		input_operating_segments.begin(), input_operating_segments.end(),
		std::inserter(input_segments_ids, input_segments_ids.begin()), get_id);

	std::vector<int> covered_segments = get_covered_operating_segments_();

	std::sort(input_segments_ids.begin(), input_segments_ids.end());
	std::sort(covered_segments.begin(), covered_segments.end());

	std::set_difference(
		input_segments_ids.begin(), input_segments_ids.end(),
		covered_segments.begin(), covered_segments.end(),
		std::inserter(uncovered_segments, uncovered_segments.begin()));

	return uncovered_segments;
}


double SolutionValidator::cost_uncovered_segments_() const
{
	return nbr_uncovered_segments_()* cost_parameters::basic_uncovered_segment_cost;
}


double SolutionValidator::cost_man_days_() const
{
	double man_day_cost  = 0;
	for (auto pairing : pairings_) {
		man_day_cost += cost_utility_.cost_man_days(pairing);
	}

	return man_day_cost;
}


double SolutionValidator::cost_layover_() const
{
	double layover_cost = 0;

	for (auto pairing : pairings_) {
		layover_cost += cost_utility_.cost_layover(pairing);

	}
	return layover_cost;
}



double SolutionValidator::cost_dhd_() const
{
	double total_dhd_cost = 0.0;

	for (auto pairing : pairings_) {
		total_dhd_cost += cost_utility_.cost_dhd(pairing);
	}

	return total_dhd_cost;
}
