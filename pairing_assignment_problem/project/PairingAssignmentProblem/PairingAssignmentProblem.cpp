// PairingAssignmentProblem.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "framework/utils/io_segments.h"

#include "framework/structs/pairing.h"

#include "framework/problem.h"
#include "framework/solution.h"
#include "framework/solution_validator.h"

#include "solver/solver.h"


int main(int argc, const char * argv[])
{

    Problem problem;
    Solution solution;

    problem.read_segments("../data/input_segments.txt");

    std::vector<Pairing *> _solution = solve(problem);

    solution.set_solution(_solution);
    solution.print();
 
    return 0;
}

